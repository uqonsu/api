FROM python:3.6-alpine

RUN adduser -D uqo

WORKDIR /home/uqo
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip && \
    apk add --no-cache zip openjdk8-jre wget && \
    unzip sonar-scanner-cli-3.2.0.1227-linux.zip || \
    echo ok && \
    rm sonar-scanner-cli-3.2.0.1227-linux.zip && \
    sed -ri 's/^use_embedded_jre=.*$/use_embedded_jre=false/' $(pwd)/sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner
ENV PATH /home/uqo/sonar-scanner-3.2.0.1227-linux/bin:$PATH
RUN apk add --no-cache gcc musl-dev python3-dev libffi-dev openssl-dev
COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY ./migrations/ ./
COPY ./uqo/ ./
COPY ./scripts ./
COPY ./manage.py ./
COPY ./.coveragerc ./
COPY ./sonar-project.properties ./

RUN chmod +x boot.sh

ENV FLASK_APP uqo

RUN chown -R uqo:uqo ./
USER uqo

EXPOSE 5000
CMD ash -c ./boot.sh