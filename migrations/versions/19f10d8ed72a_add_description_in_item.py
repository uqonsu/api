"""add description in item

Revision ID: 19f10d8ed72a
Revises: 6e8335d6ed34
Create Date: 2019-01-18 17:18:46.953740

"""

# revision identifiers, used by Alembic.
revision = '19f10d8ed72a'
down_revision = '6e8335d6ed34'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('items', sa.Column('description', sa.String(), nullable=False, server_default=''))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('items', 'description')
    # ### end Alembic commands ###
