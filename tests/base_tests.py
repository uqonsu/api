﻿from flask_testing import TestCase
import unittest

from uqo import create_app, db
from uqo.models.models import User, Code


class MyTest(TestCase):
    TESTING = True

    def create_app(self):
        # pass in test configuration
        return create_app('test')

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_default(self):
        # print(app.config)
        user = User(self.app.config['TWILIO_TEST_USER_NUMBER'])
        db.session.add(user)
        db.session.commit()

        # this works
        assert user in db.session

    def test_registration(self):
        number = self.app.config['TWILIO_TEST_USER_NUMBER']
        r1 = self.client.post('/customer/auth', data=dict(phone_number=number))
        self.assert200(r1)
        token = r1.json()['token']

        user = db.session.query(User, User.phone_number, number)
        code = db.session.query(Code, Code.user_id, user.id)
        r2 = self.client.post('/customer/verify', data=dict(
            token=token, verification_code=code))
        self.assert200(r2)
