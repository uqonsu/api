import os

from flask import send_file
from werkzeug.utils import secure_filename

from uqo import app, login_manager, api
from uqo.models.models import User
from uqo.services.auth_service import AuthService


@app.route('/')
@app.route('/home')
def home():
    return "hello world!"


@app.route(app.config['IMAGES_URI'] + '<image>')
def image(image):
    return send_file(os.path.abspath(os.path.join(app.config['IMAGES_FOLDER'], secure_filename(image))))


@login_manager.request_loader
def load_user_from_request(request):
    auth_service = AuthService()
    auth_data = request.headers.get('Authorization')
    if auth_data:
        splited = auth_data.rsplit(maxsplit=1)
        if len(splited) == 2 and splited[0].encode('utf-8') == b'Bearer':
            token = splited[1].encode('utf-8')
            user = auth_service.getUserByToken(token)
            if isinstance(user, User):
                return user
    return None


@login_manager.unauthorized_handler
def unauthorized(message='unauthorized'):
    return {
        'success': False,
        'message': message
    }, 401, {
        'WWW-Authenticate': 'Bearer'
    }


@api.errorhandler
def default_error_handler(error):
    """Default error handler"""
    return {'message': str(error)}, getattr(error, 'code', 500)


import uqo.customers
import uqo.orders
import uqo.organizations
import uqo.shops
import uqo.workers

api.add_namespace(uqo.customers.api, path='/customer')
api.add_namespace(uqo.orders.api, path='/order')
api.add_namespace(uqo.organizations.api, path='/organization')
api.add_namespace(uqo.shops.api, path='/shop')
api.add_namespace(uqo.workers.api, path='/worker')
