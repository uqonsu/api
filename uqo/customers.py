import stripe
from flask_login import current_user
from flask_restplus import Resource, reqparse, Namespace, fields

from uqo import db
from uqo.models.models import Customer, OrderStatus, Order, User, OrderItem, Item
from uqo.services.auth_service import AuthService, login_required
import uqo.utilities

api = Namespace('customers', 'Customers related methods')


@api.route('/issue_key')
class IssueKey(Resource):
    stripe_version = api.model('StripeVersion', {
        'stripe_version': fields.String(required=True)
    })

    @login_required('customers')
    @api.expect(stripe_version)
    def post(self):
        stripe_version = api.payload['stripe_version']
        customer_id = current_user.stripe_token
        key = stripe.EphemeralKey.create(customer=customer_id, stripe_version=stripe_version)
        return key


@api.route('/auth')
class Auth(Resource):
    phone_number = api.model('PhoneNumber', {
        'phone_number': fields.String(required=True, desciption='phone number in format acceptable by twilio')
    })

    @api.doc(security=None)
    @api.expect(phone_number)
    def post(self):
        args = api.payload
        user = Customer.query.filter(Customer.phone == args['phone_number']).one_or_none()
        if user is None:
            customer = stripe.Customer.create()
            user = Customer(
                phone=args['phone_number'],
                stripe_token=customer.stripe_id
            )
        db.session.add(user)
        db.session.commit()

        authy_services = AuthService()
        token = authy_services.request_phone_confirmation_code(user)
        if token:
            db.session.commit()
            return {'token': token.decode('utf-8')}, 200

        return {}, 501


@api.route('/order')
@api.doc(description='orders history')
class Auth(Resource):
    @login_required('customers')
    def get(self):
        orders = []
        last_order = None
        for order, status, order_item, item in db.session \
            .query(Order, OrderStatus, OrderItem, Item) \
            .join(Customer) \
            .join(OrderStatus) \
            .join(OrderItem)\
            .join(Item) \
            .filter(Customer.id == current_user.id) \
            .order_by(Order.id.desc()):
            if last_order != order.id:
                last_order = order.id
                orders.append({
                    'id': str(order.id),
                    'shop_id': str(order.shop_id),
                    'worker_id': str(order.worker_id),
                    'customer_id': str(order.customer_id),
                    'status': order.status,
                    'created': order.created.isoformat() if order.created is not None else '',
                    'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                    'order_name': order.order_name,
                    'order_paid': order.order_paid,
                    'items': []
                })
            orders[-1]['items'].append({
                    "id": item.id,
                    "name": item.name,
                    "price": int(order_item.unit_price * 100),
                    "amount": order_item.amount,
                    'description': item.description,
                    'image': item.image.dict()

            })
        return {"orders": orders}, 200


@api.route('/verify')
class Verify(Resource):
    verification = api.model('AuthVerification', {
        'token': fields.String(required=True, description='verification token'),
        'code': fields.String(required=True, description='code from sms')
    })

    @api.expect(verification)
    def post(self):
        args = api.payload
        auth_service = AuthService()
        try:
            token = auth_service.confirm_phone_number(args['token'].encode('utf-8'), args['code'])
            if token:
                return {'token': token.decode('utf-8')}, 200
            else:
                return uqo.utilities.Verification_Unsuccessful, 401
        except ValueError as e:
            return print(e), 401

