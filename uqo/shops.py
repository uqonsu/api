from flask import abort
from flask_login import current_user
from flask_restplus import Resource, abort, reqparse, Namespace, fields
from werkzeug.datastructures import FileStorage

from uqo import db, images
from uqo.models.models import Shop, Organization, OrderStatus, Order, ShopItem, Item, Image
from uqo.services.auth_service import login_required
from flask_uploads import IMAGES

api = Namespace('shops', 'Shops related methods')


@api.route('/')
class ShopsResource(Resource):
    @login_required
    def get(self):
        q = db.session.query(Shop, Organization)
        records = q.join(Organization, Shop.organization_id == Organization.id)

        return {'shops': [{
            'id': str(shop.id),
            'description': shop.description or '',
            'organization_id': str(org.id),
            'organization_name': org.name,
            'online': shop.online.isoformat(),
            'image': shop.image.dict()

        } for shop, org in records]}, 200


@api.route('/<string:shop_id>')
class ShopIdEnd(Resource):
    @login_required
    def get(self, shop_id):
        shop: Shop = db.session.query(Shop).filter(Shop.id == int(shop_id)).one_or_none()
        if shop is None:
            abort(501, 'No such shop')
        org = db.session.query(Organization).filter(Organization.id == shop.organization_id).one_or_none()
        if org is None:
            abort(501, 'No such organization')

        return {
                   'id': str(shop.id),
                   'description': shop.description or '',
                   'organization_id': str(shop.organization_id),
                   'organization_name': org.name,
                   'online': shop.online.isoformat(),
                   'image': shop.image.dict()
               }, 200

    image_parser = api.parser()
    image_parser.add_argument('image', location='files',
                              type=FileStorage, required=True)

    @api.expect(image_parser)
    @login_required('workers')
    def patch(self, shop_id):
        shop = Shop.query.get(shop_id)
        if shop.organization_id != current_user.organization_id:
            abort(403, 'shop from another organization')
        args = self.image_parser.parse_args()
        image_file = args['image']
        path = images.save(image_file)
        tmp = None
        if shop.image_id is not None:
            tmp = shop.image_id
        image = Image.create(path)
        try:
            shop._image = image
            db.session.commit()
        except Exception as e:
            Image.remove(image)
            print(e)
        if tmp is not None:
            Image.remove(tmp)
        db.session.commit()
        return image.id, 200


@api.route('/<string:shop_id>/items')
class ShopItemsEnd(Resource):
    @login_required
    def get(self, shop_id):
        int_id = int(shop_id)
        q = db.session.query(ShopItem, Item). \
            join(Item, Item.id == ShopItem.item).filter(ShopItem.shop_id == int_id)

        return {'items': [{
            'id': str(item.id),
            'organization_id': str(item.organization_id),
            'name': item.name,
            'price': int(item.price * 100),
            'description': item.description,
            'image': item.image.dict()
        } for shop_item, item in q]}, 200


@api.route('/<string:shop_id>/orders/')
class ShopOrdersEnd(Resource):
    filter_parser = api.parser()
    filter_parser.add_argument('filter', type=str, help='comma-separated statuses of orders to be returned', location='args')

    @login_required
    @api.expect(filter_parser)
    def get(self, shop_id):
        args = self.filter_parser.parse_args()
        filter = [arg for arg in args['filter'].split(',') if arg] if args['filter'] is not None else None
        int_id = int(shop_id)
        status_ids = db.session.query(OrderStatus)
        if filter:
            status_ids = status_ids.filter(OrderStatus.name.in_(filter))
        status_ids = [status.id for status in status_ids]

        q = db.session.query(Order) \
            .filter(Order.shop_id == int_id) \
            .filter(Order.status.in_(status_ids))

        return {'orders': [{
            'id': str(order.id),
            'shop_id': str(order.shop_id),
            'worker_id': str(order.worker_id),
            'customer_id': str(order.customer_id),
            'status': order.status,
            'created': order.created.isoformat() if order.created is not None else '',
            'order_time': order.order_time.isoformat() if order.order_time is not None else '',
            'order_name': order.order_name,
            'order_paid': order.order_paid
        } for order in q]}, 200


@api.route('/items/<string:item_id>')
class ItemResource(Resource):
    image_parser = api.parser()
    image_parser.add_argument('image', location='files',
                              type=FileStorage, required=True)

    @api.expect(image_parser)
    @login_required('workers')
    def patch(self, item_id):
        item = Item.query.get(item_id)
        if item.organization_id != current_user.organization_id:
            abort(403, 'item from another shop')
        args = self.image_parser.parse_args()
        image_file = args['image']
        path = images.save(image_file)
        tmp = None
        if item.image_id is not None:
            tmp = item.image_id
        image = Image.create(path)
        try:
            item._image = image
            db.session.commit()
        except Exception as e:
            Image.remove(image)
            print(e)
        if tmp is not None:
            Image.remove(tmp)
        db.session.commit()
        return image.id, 200

