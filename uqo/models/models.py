﻿import os

from flask import url_for
from sqlalchemy.ext.declarative import declared_attr

from uqo import db, bcrypt, app, api
import flask_restplus
from werkzeug.security import generate_password_hash, \
     check_password_hash
from PIL import Image as PILImage


class ApiBase(db.Model):
    __abstract__ = True
    _type_field_dict = {
        db.String.__name__: flask_restplus.fields.String,
        db.Integer.__name__: flask_restplus.fields.Integer,
        db.Boolean.__name__: flask_restplus.fields.Boolean,
        db.DateTime.__name__: flask_restplus.fields.DateTime,
        db.Float.__name__: flask_restplus.fields.Float
    }

    def get_api_model(self):
        cols = self.__class__.__table__.columns
        return api.model(self.__class__.__name__, {
            x.name: self._type_field_dict[x.type.__class__.__name__] for x in cols
        })


class Image(db.Model):
    __tablename__ = 'images'
    id = db.Column(db.Integer, primary_key=True)
    small = db.Column(db.String)
    medium = db.Column(db.String)
    original = db.Column(db.String, nullable=False)

    @staticmethod
    def create(path):
        folder = app.config['IMAGES_FOLDER']
        original = PILImage.open(os.path.abspath(os.path.join(folder, path)))
        splitpath = path.rsplit('.', 1)
        ratio = min(500 / original.width, 500 / original.height)
        if ratio < 1:
            medium = original.resize((int(original.width * ratio), int(original.height * ratio)), PILImage.LANCZOS)
            mediumpath = splitpath[0] + '-medium.' + splitpath[1]
            medium.save(os.path.abspath(os.path.join(folder, mediumpath)), optimize=True, quality=95)
        else:
            mediumpath = path
        ratio = min(100 / original.width, 100 / original.height)
        if ratio < 1:
            small = original.resize((int(original.width * ratio), int(original.height * ratio)), PILImage.LANCZOS)
            smallpath = splitpath[0] + '-small.' + splitpath[1]
            small.save(os.path.abspath(os.path.join(folder, smallpath)), optimize=True, quality=95)
        else:
            smallpath = path
        return Image(small=url_for('image', image=smallpath), medium=url_for('image', image=mediumpath),
                     original=url_for('image', image=path))

    @staticmethod
    def remove(image_id):
        pass

    def get_small(self):
        url = self.small or self.medium or self.original
        return url if url[0] != '/' else app.config['URL'] + url

    def get_medium(self):
        url = self.medium or self.original
        return url if url[0] != '/' else app.config['URL'] + url

    def get_original(self):
        url = self.original
        return url if url[0] != '/' else app.config['URL'] + url

    def dict(self):
        return {
            'small': self.get_small(),
            'medium': self.get_medium(),
            'original': self.get_original()
        }


class ImageMixin:
    @declared_attr
    def image_id(cls):
        return db.Column(db.Integer, db.ForeignKey('images.id', ondelete='SET NULL'))

    @declared_attr
    def _image(cls):
        return db.relationship('Image')

    @property
    def _default_image(self):
        default_path = 'https://avatars0.githubusercontent.com/u/14371067?s=460&v=4' if hash(
            self.id) % 2 == 0 else 'https://avatars2.githubusercontent.com/u/15768502?s=88&v=4'
        return Image(original=default_path)

    @property
    def image(self):
        if self._image is None:
            return self._default_image
        else:
            return self._image


class User(ApiBase):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    active_since = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '<User {}'.format(self.id)

    @classmethod
    def roles(cls):
        roles = set()
        for base in (cls, *cls.__bases__):
            if hasattr(base, '__tablename__'):
                roles.add(base.__tablename__)

        return roles


class Code(ApiBase):
    __tablename__ = "codes"

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    access_code = db.Column(db.String, nullable=False)
    creation_time = db.Column(db.DateTime, nullable=False, primary_key=True, server_default=db.func.now())

    def __init__(self, user_id, access_code):
        self.user_id = user_id
        self.access_code = access_code

    def get_id(self):
        return str(self.user_id)

    def __unicode__(self):
        return self.access_code

    def __repr__(self):
        return 'Code: {}'.format(self.access_code)


class Organization(ApiBase):
    __tablename__ = "organizations"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=True)
    account_token = db.Column(db.String, nullable=True)

    def __init__(self, name):
        self.name = name

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return 'Organization: {{name: {} id: {} }}'.format(self.name, self.id)


class Shop(ImageMixin, ApiBase):
    __tablename__ = "shops"

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String, nullable=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organizations.id'))
    online = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    def __init__(self, organization_id):
        self.organization_id = organization_id

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '''Shop: 
        id = {}, description = {}, organization_id = {}, online = {}
        '''.format(self.id, self.description, self.organization_id, self.online)


class Customer(User):
    __tablename__ = "customers"
    __mapper_args__ = {'polymorphic_identity': 'customers'}

    id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    phone = db.Column(db.String, unique=True)
    name = db.Column(db.String)
    stripe_token = db.Column(db.String)

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '''Customer: {{
        id = {}, phone = {}, name = {}, stripe_token = {}
        }}'''.format(self.id, self.phone, self.name, self.stripe_token)


class Worker(User):
    __tablename__ = "workers"
    __mapper_args__ = {'polymorphic_identity': 'workers'}

    id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    name = db.Column(db.String)
    organization_id = db.Column(db.Integer, db.ForeignKey('organizations.id'))
    user = db.Column(db.String, unique=True, nullable=False)
    pass_hash = db.Column(db.String, nullable=False)

    def set_password(self, password):
        # TODO shop admin
        # self.pass_hash = generate_password_hash(password)
        self.pass_hash = password

    def check_password(self, password):
        # TODO shop admin
        # return check_password_hash(self.pass_hash, password)
        return self.pass_hash == password

    def __init__(self, name, organization_id, user, password):
        self.name = name
        self.organization_id = organization_id
        self.user = user
        self.set_password(password)

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '''Worker: {{
        id = {}, name = {}, organization_id = {}, user = {}, pass_hash = {}
        }}'''.format(self.id, self.name, self.organization_id, self.user, self.pass_hash)


class Item(ImageMixin, ApiBase):
    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organizations.id'))
    name = db.Column(db.String)
    description = db.Column(db.String, nullable=False, server_default=db.text(''))
    price = db.Column(db.Float)

    def __init__(self, organization_id, name, price):
        self.organization_id = organization_id
        self.name = name
        self.price = price

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '''
        Item: {{id = {}, organization_id = {}, name = {}, price = {}
        }}'''.format(self.id, self.organization_id, self.name, self.price)


class ShopItem(ApiBase):
    __tablename__ = "shop_item"

    shop_id = db.Column(db.Integer, db.ForeignKey('shops.id'), primary_key=True)
    item = db.Column(db.Integer, db.ForeignKey('items.id'), primary_key=True)
    available = db.Column(db.Boolean, server_default=db.text('false'))

    def __init__(self, shop_id, item):
        self.shop_id = shop_id
        self.item = item

    def get_id(self):
        return str(self.shop_id)

    def __unicode__(self):
        return self.shop_id

    def __repr__(self):
        return '''
        ShopItem: {{shop_id = {}, item = {}, available = {}
        }}'''.format(self.shop_id, self.item, self.available)


class OrderStatus(ApiBase):
    __tablename__ = "order_status"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    description = db.Column(db.String, nullable=True)

    def __init__(self, name, description=''):
        self.name = name
        self.description = description

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return 'OrderStatus: {{id = {}, name = {}, description = {}}}'.format(self.id, self.name, self.description)


class Order(ApiBase):
    __tablename__ = "orders"

    id = db.Column(db.Integer, primary_key=True)
    shop_id = db.Column(db.Integer, db.ForeignKey('shops.id'))
    worker_id = db.Column(db.Integer, db.ForeignKey('workers.id'))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    status = db.Column(db.Integer, db.ForeignKey('order_status.id'))
    created = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    order_time = db.Column(db.DateTime)
    order_name = db.Column(db.String)
    order_paid = db.Column(db.Boolean, server_default=db.text('false'))
    payment_token = db.Column(db.String, nullable=True)

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return 'Order object'


class OrderItem(ApiBase):
    __tablename__ = "order_item"

    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'))
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'))
    amount = db.Column(db.Integer, nullable=False, server_default=db.text('0'))
    unit_price = db.Column(db.Float)

    def __init__(self, item_id, order_id, amount, unit_price):
        self.item_id = item_id
        self.order_id = order_id
        self.amount = amount
        self.unit_price = unit_price

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    def __repr__(self):
        return '''
        OrderItem: {{
        id = {}, item_id = {}, order_id = {}
        amount = {}, unit_price = {}
        }}'''.format(self.id, self.item_id, self.order_id,
                     self.amount, self.unit_price)
