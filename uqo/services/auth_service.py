﻿import collections
from functools import wraps

from flask import request, current_app
from flask_login import current_user, login_required as lr
from flask_login.config import EXEMPT_METHODS
from twilio.rest.api.v2010.account.message import MessageInstance

import uqo.utilities
from uqo import db, app
from uqo.utilities.settings import TwilioSettings
from twilio.rest import Client
from uqo.models.models import Code, User, Customer, Worker
import secrets
import jwt
from datetime import datetime, timedelta


class AuthService:
    twilio_client = None
    algorithm = 'HS256'

    def __init__(self):
        if AuthService.twilio_client is None:
            AuthService.twilio_client = Client(TwilioSettings.account_sid(), TwilioSettings.auth_token())

    def send_registration_success_sms(self, to_number):
        message = AuthService.twilio_client.messages.create(
            body=uqo.utilities.Signup_Complete,
            to=to_number,
            from_=TwilioSettings.phone_number())
        return message

    def request_phone_confirmation_code(self, user):
        if user is None:
            raise ValueError(uqo.utilities.User_Id_Not_Found)
        code = Code(user_id=user.id, access_code='{0:04d}'.format(int(secrets.token_hex(2), 16))[:4])
        db.session.add(code)
        message: MessageInstance = AuthService.twilio_client.messages.create(
            body=code.access_code,
            to=user.phone,
            from_=TwilioSettings.phone_number()
        )
        return jwt.encode({'phone_number': user.phone, 'code': code.access_code},
                          app.config['SECRET_KEY'], algorithm=AuthService.algorithm)

    def confirm_phone_number(self, token, verification_code):
        decoded = jwt.decode(token,
                             app.config['SECRET_KEY'], algorithm=AuthService.algorithm)
        if decoded.get('code') == verification_code:
            user = Customer.query.filter(Customer.phone == decoded.get('phone_number', None)).all()
            if not user:
                raise ValueError(uqo.utilities.User_Id_Not_Found)
            user = user[0]
            since = datetime.utcnow() - timedelta(minutes=10)
            for code in Code.query.filter(verification_code == Code.access_code, Code.creation_time >= since):
                if code.user_id == user.id:
                    user.phone_number_confirmed = True
                    return jwt.encode({
                        'id': str(user.id),
                        'phone_number': user.phone,
                        'code': verification_code,
                        'since': str(datetime.utcnow().isoformat())
                    }, app.config['SECRET_KEY'], algorithm=AuthService.algorithm)
        return None

    def getUserByToken(self, token):
        decoded = jwt.decode(token.decode('utf-8'), app.config['SECRET_KEY'], algorithm=AuthService.algorithm)
        user_id = decoded['id']
        user = Customer.query.get(user_id)
        if user and user.phone == decoded['phone_number']:
            return user
        user = Worker.query.get(user_id)
        if user and user.user == decoded['username']:
            return user
        return None


def login_required(*roles):
    @wraps(login_required)
    def decorator(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if roles and hasattr(current_user, 'roles') and not (current_user.roles() & set(roles)):
                return current_app.login_manager.unauthorized()
            return lr(func)(*args, **kwargs)
        return decorated_view
    if len(roles) == 1 and isinstance(roles[0], collections.Callable):
        func = roles[0]
        roles = ()
        return decorator(func)
    return decorator
