
from twilio.rest.api.v2010.account.message import MessageInstance

import uqo.utilities
from uqo.utilities.settings import TwilioSettings
from twilio.rest import Client


class OrderService:
    twilio_client = None
    algorithm = 'HS256'

    def __init__(self):
        if OrderService.twilio_client is None:
            OrderService.twilio_client = Client(TwilioSettings.account_sid(), TwilioSettings.auth_token())

    def send_phone_message(self, user, text):
        if user is None:
            raise ValueError(uqo.utilities.User_Id_Not_Found)

        message: MessageInstance = OrderService.twilio_client.messages.create(
            body=text,
            to=user.phone,
            from_=TwilioSettings.phone_number()
        )
        return True
