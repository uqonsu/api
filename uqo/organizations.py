from flask_login import current_user


from uqo.models.models import Shop, Organization, Worker
from uqo.services.auth_service import login_required
from flask_restplus import Resource, Namespace, fields, abort

api = Namespace('organizations', 'Organizations related methods')


@api.route('/')
class OrganizationsResource(Resource):
    organization_id = api.parser()
    organization_id.add_argument('id', type=str, location='args', help='organization id')

    def get_info(self, id):
        org = Organization.query.get(str(id))
        if not org:
            abort(404, 'No such organization')
        return {
            'id': str(org.id),
            'name': org.name,
            'shops': [
                {
                    "id": str(shop.id),
                    "description": shop.description,
                    "online": shop.online.isoformat(),
                    'image': shop.image.dict()
                }
                for shop in Shop.query.join(Organization).filter(Shop.organization_id == org.id)
            ]
        }

    @login_required('workers')
    def get_worker(self):
        worker: Worker = current_user
        id = str(worker.organization_id)
        return self.get_info(id)

    @login_required
    @api.expect(organization_id)
    def get(self):
        id = self.organization_id.parse_args()['id']
        if id:
            return self.get_info(id)
        else:
            return self.get_worker()
