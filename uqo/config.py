﻿from os import environ as env

from flask_uploads import UploadSet, IMAGES

authorizations = {
    'BearerToken': {
        'type': 'apiKey',
        'name': 'Authorization',
        'in': 'header'
    }
}
images = UploadSet('images', IMAGES, default_dest=lambda app: app.config['IMAGES_FOLDER'])


class DefaultConfig(object):
    SECRET_KEY = '%^!@@*!&$8xdfdirunb52438#(&^874@#^&*($@*(@&^@)(&*)Y_)((+'
    SQLALCHEMY_DATABASE_URI = env.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    RESTPLUS_VALIDATE = True
    IMAGES_FOLDER = './images'
    IMAGES_URI = '/public_data/images/'
    URL = env.get('API_URL')


class DevelopmentConfig(DefaultConfig):
    AUTHY_KEY = env.get('AUTHY_KEY')

    TWILIO_ACCOUNT_SID = env.get('TWILIO_ACCOUNT_SID')
    TWILIO_AUTH_TOKEN = env.get('TWILIO_AUTH_TOKEN')
    TWILIO_NUMBER = env.get('TWILIO_NUMBER')
    STRIPE_PUBLISHABLE_KEY = env.get('STRIPE_PUBLISHABLE_KEY')
    STRIPE_SECRET_KEY = env.get('STRIPE_SECRET_KEY')

    DEBUG = True


class TestConfig(DefaultConfig):
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    TWILIO_ACCOUNT_SID = 'AC3fd279a4a4ded3b1dfe0908669a7d9c8'
    TWILIO_AUTH_TOKEN = '8cf2322617473677bfae8953af08aad6'
    TWILIO_NUMBER = '+15005550006'
    TWILIO_TEST_USER_NUMBER = '+15005550006'

    DEBUG = True
    TESTING = True
    WTF_CSRF_ENABLED = False


config_env_files = {
    'test': 'uqo.config.TestConfig',
    'development': 'uqo.config.DevelopmentConfig',
}
