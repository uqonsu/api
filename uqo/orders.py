import dateutil.parser
import stripe
import stripe.error
from flask import abort
from flask_login import current_user
from flask_restplus import Resource, abort, fields, Namespace
from uqo import db
from uqo.models.models import User, Shop, Organization, Customer, OrderStatus, Order, OrderItem, ShopItem, Item, Worker
from uqo.services.auth_service import AuthService, login_required
from uqo.services.order_service import OrderService

api = Namespace('orders', 'Orders related methods')


@api.route('/<string:order_id>/payment')
class OrderResource(Resource):
    @login_required
    def post(self, order_id):
        if 'customers' in current_user.roles():
            order_org = db.session.query(Order, Organization).filter(Order.id == order_id).join(Shop).join(
                Organization).filter(Order.customer_id == current_user.id).one_or_none()
            if order_org is None:
                return {'error': 'no such order'}, 400
            order, org = order_org
            if order.order_paid:
                return {'warning': 'order already paid'}, 200
            amount = sum(int((order_item.unit_price * order_item.amount) * 100)
                         for order_item in OrderItem.query.filter(OrderItem.order_id == order_id))
            try:
                charge = stripe.Charge.create(
                    amount=amount,
                    currency="rub",
                    customer=current_user.stripe_token,
                    destination={
                        # "amount": amount * percent,
                        "account": org.account_token,
                    }
                )
                order.order_paid = True
                order.payment_token = charge.id
                db.session.commit()
            except stripe.error.CardError as e:
                return {'error': str(e)}, 400
            return charge
        elif 'workers' in current_user.roles():
            order_org = db.session.query(Order, Organization).filter(Order.id == order_id).join(Shop).join(
                Organization).filter(Order.worker_id == current_user.id).one_or_none()
            if order_org is None:
                return {'error': 'no such order'}, 400
            order, org = order_org
            if order.order_paid:
                return {'warning': 'order already paid'}, 200
            order.order_paid = True
            db.session.commit()
            return {}, 200
        else:
            print(type(current_user))
            return {'error': 'wrong user type'}, 403


@api.route('/<string:id>/items/')
class OrderItemsEnd(Resource):
    @login_required
    def get(self, id):
        return {"items": [
            {
                "id": item.id,
                "name": item.name,
                "price": int(order_item.unit_price * 100),
                "amount": order_item.amount,
                'description': item.description,
                'image': item.image.dict()
            } for item, order_item in db.session.query(Item, OrderItem).join(OrderItem).filter(OrderItem.order_id == id)
        ]}


@api.route('/<string:id>/cancel')
class ReceiveOrderEnd(Resource):
    _status_name_exception = 'completed'
    _status_name_cancelled = 'cancelled'

    @login_required
    def post(self, id):
        int_id = int(id)
        order = db.session.query(Order).filter(Order.id == int_id).one_or_none()
        if order is None:
            abort(501, 'Did not find this order')

        # organization checking
        shop = db.session.query(Shop).filter(Shop.id == order.shop_id).one_or_none()
        if shop is None:
            abort(501, "Cannot find this shop")
        if 'workers' in current_user.roles():
            role = 'worker'
            if shop.organization_id != current_user.organization_id:
                abort(501, "You are from a different organization, you don't have a permission")
        else:
            role = 'customer'
            if current_user.id != order.customer_id:
                abort(501, "This is not your order, you can't cancel it")

        # status checking
        cur_status = db.session.query(OrderStatus). \
            filter(OrderStatus.id == order.status).one_or_none()
        if cur_status is None:
            abort(501, "Server error, cannot find order's status")
        if cur_status.name == self._status_name_exception:
            abort(501, "This order is completed. Nothing to cancel")

        next_status = db.session.query(OrderStatus). \
            filter(OrderStatus.name == self._status_name_cancelled).one_or_none()
        if next_status is None:
            abort(501, "Server error, cannot find cancelled status")
        order.status = next_status.id

        user = db.session.query(Customer).filter(Customer.id == order.customer_id).one_or_none()
        if user is None:
            db.session.rollback()
            abort(501, 'Cannot find this customer to send him a message')

        # sending a message
        db.session.commit()
        order_service = OrderService()
        order_service.send_phone_message(user, text='Order {} was cancelled by a {}'.format(order.order_name, role))

        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid
               }, 200


@api.route('/<string:id>/finish')
class ReceiveOrderEnd(Resource):
    _status_name_confirmed = 'confirmed'
    _status_name_finished = 'ready'

    @login_required('workers')
    def post(self, id):
        int_id = int(id)
        order = db.session.query(Order).filter(Order.id == int_id).one_or_none()
        if order is None:
            abort(501, 'Did not find this order')

        # organization checking
        shop = db.session.query(Shop).filter(Shop.id == order.shop_id).one_or_none()
        if shop is None:
            abort(501, "Cannot find this shop")
        if shop.organization_id != current_user.organization_id:
            abort(501, "You are from a different organization, you don't have a permission")

        # status checking
        cur_status = db.session.query(OrderStatus). \
            filter(OrderStatus.id == order.status).one_or_none()
        if cur_status is None:
            abort(501, "Server error, cannot find order's status")
        if cur_status.name != self._status_name_confirmed:
            abort(501, "This order's status is not 'confirmed'")

        next_status = db.session.query(OrderStatus). \
            filter(OrderStatus.name == self._status_name_finished).one_or_none()
        if next_status is None:
            abort(501, "Server error, cannot find 'ready' status")

        # update order info
        order.status = next_status.id

        # sending a massage
        user = db.session.query(Customer).filter(Customer.id == order.customer_id).one_or_none()
        if user is None:
            db.session.rollback()
            abort(501, 'Cannot find this customer to send him a message')
        db.session.commit()

        order_service = OrderService()
        order_service.send_phone_message(user, text='Order {} is ready'.format(order.order_name))

        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid
               }, 200


@api.route('/<string:id>/receive')
class ReceiveOrderEnd(Resource):
    _status_name_pending = 'pending'
    _status_name_confirmed = 'confirmed'

    @login_required('workers')
    def post(self, id):
        int_id = int(id)
        order = db.session.query(Order).filter(Order.id == int_id).one_or_none()
        if order is None:
            abort(501, 'Did not find this order')

        # organization checking
        shop = db.session.query(Shop).filter(Shop.id == order.shop_id).one_or_none()
        if shop is None:
            abort(501, "Cannot find this shop")
        if shop.organization_id != current_user.organization_id:
            abort(501, "You are from a different organization, you don't have a permission")

        # status checking
        cur_status = db.session.query(OrderStatus). \
            filter(OrderStatus.id == order.status).one_or_none()
        if cur_status is None:
            abort(501, "Server error, cannot find order's status")
        if cur_status.name != self._status_name_pending:
            abort(501, "This order's status is not 'pending'")

        next_status = db.session.query(OrderStatus). \
            filter(OrderStatus.name == self._status_name_confirmed).one_or_none()
        if next_status is None:
            abort(501, 'Server error, cannot find confirmed status')

        # update order info
        order.status = next_status.id
        order.worker_id = current_user.id

        # sending a massage
        user = db.session.query(Customer).filter(Customer.id == order.customer_id).one_or_none()
        if user is None:
            db.session.rollback()
            abort(501, 'Cannot find this customer to send him a message')
        db.session.commit()

        order_service = OrderService()
        order_service.send_phone_message(user, text='Order {} was received'.format(order.order_name))

        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid
               }, 200


@api.route('/<string:id>/done')
class ReceiveOrderEnd(Resource):
    _status_name_ready = 'ready'
    _status_name_completed = 'completed'

    @login_required('workers')
    def post(self, id):
        int_id = int(id)
        order = db.session.query(Order).filter(Order.id == int_id).one_or_none()
        if order is None:
            abort(501, 'Did not find this order')
        if not order.order_paid:
            abort(501, "Order is not paid")
        # organization checking
        shop = db.session.query(Shop).filter(Shop.id == order.shop_id).one_or_none()
        if shop is None:
            abort(501, "Cannot find this shop")
        if shop.organization_id != current_user.organization_id:
            abort(501, "You are from a different organization, you don't have a permission")

        # status checking
        cur_status = db.session.query(OrderStatus). \
            filter(OrderStatus.id == order.status).one_or_none()
        if cur_status is None:
            abort(501, "Server error, cannot find order's status")
        if cur_status.name != self._status_name_ready:
            abort(501, "This order's status is not '{}'".format(self._status_name_ready))

        next_status = db.session.query(OrderStatus). \
            filter(OrderStatus.name == self._status_name_completed).one_or_none()
        if next_status is None:
            abort(501, 'Server error, cannot find confirmed status')

        # update order info
        order.status = next_status.id

        # sending a massage
        user = db.session.query(Customer).filter(Customer.id == order.customer_id).one_or_none()
        if user is None:
            db.session.rollback()
            abort(501, 'Cannot find this customer to send him a message')
        db.session.commit()

        order_service = OrderService()
        order_service.send_phone_message(user, text='Order {} was successfully completed'.format(order.order_name))

        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid
               }, 200


@api.route('/')
class OrderResource(Resource):
    item_fields = api.model('OrderItem', {
        'id': fields.String(required=True),
        'amount': fields.Integer(required=True, min=1)
    })

    order_model = api.model('Order', {
        'shop_id': fields.String(description='Application name', required=True),
        'time': fields.String(description='Application name', required=True),
        'items': fields.List(fields.Nested(item_fields), required=True)
    })

    @login_required('customers')
    @api.expect(order_model)
    def post(self):
        data = api.payload
        shop_id = int(data['shop_id'])
        time = dateutil.parser.parse(data['time'])

        # creating order object
        order = Order()
        order.shop_id = shop_id
        order.customer_id = current_user.id
        order.order_time = time
        order.order_paid = False
        pending_status = db.session.query(OrderStatus).filter(OrderStatus.name == 'pending').one_or_none()
        if pending_status is None:
            abort(501, "Can't find pending status")
        order.status = pending_status.id

        db.session.add(order)
        db.session.flush()
        order_id = order.id
        order.order_name = chr(ord('A') + ((order_id // 100) % 26)) + str(order_id % 100)

        # adding all order_item's
        for item in data['items']:
            item_id = item['id']
            item_amount = item['amount']

            # availability checking
            shop_item = db.session.query(ShopItem).filter(
                ShopItem.shop_id == shop_id,
                ShopItem.item == item_id
            ).one_or_none()
            if shop_item is None or not shop_item.available:
                db.session.rollback()
                db.session.remove(order)
                db.session.commit()
                abort(400, 'item {} is not available in this shop'.format(item_id))

            item_obj = db.session.query(Item).filter(Item.id == item_id).one_or_none()
            order_item = OrderItem(item_id, order_id, item_amount, item_obj.price)
            db.session.add(order_item)
        db.session.commit()
        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid
               }, 200


@api.route('/<string:order_id>')
class OrderResourceItem(Resource):
    @login_required
    def get(self, order_id):
        order = Order.query.filter(Order.id == order_id).one_or_none()
        if order is None:
            return {'error': 'no such order'}, 400
        return {
                   'id': str(order.id),
                   'shop_id': str(order.shop_id),
                   'worker_id': str(order.worker_id),
                   'customer_id': str(order.customer_id),
                   'status': order.status,
                   'created': order.created.isoformat() if order.created is not None else '',
                   'order_time': order.order_time.isoformat() if order.order_time is not None else '',
                   'order_name': order.order_name,
                   'order_paid': order.order_paid,
                   'items': OrderItemsEnd(api=api).get(order_id)['items']
               }, 200
