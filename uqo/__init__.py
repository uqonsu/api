﻿from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_restplus import Api
from uqo.config import config_env_files, authorizations, images
import stripe
from flask_uploads import configure_uploads
db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
api = Api(authorizations=authorizations, security='BearerToken')


def create_app(config_name='development',
               p_db=db,
               p_bcrypt=bcrypt,
               p_login_manager=login_manager,
               p_api=api,
               p_strict_slashes=False):
    new_app = Flask(__name__)
    new_app.config.from_object(config_env_files[config_name])
    p_db.init_app(new_app)
    p_bcrypt.init_app(new_app)
    p_login_manager.init_app(new_app)
    p_login_manager.login_view = 'customer_auth'
    p_api.init_app(new_app)
    new_app.url_map.strict_slashes = p_strict_slashes
    stripe.api_key = new_app.config['STRIPE_SECRET_KEY']
    configure_uploads(new_app, (images,))
    return new_app


app = create_app()
import uqo.general_imports
