from datetime import datetime
from flask_restplus import Resource, reqparse, Namespace, fields
import jwt

from uqo import app
from uqo.models.models import Worker
from uqo.services.auth_service import AuthService


api = Namespace('workers', 'Workers related methods')


@api.route('/auth')
class WorkerAuth(Resource):
    worker_auth = api.model('WorkerAuth', {
        'username': fields.String(required=True),
        'password': fields.String(required=True)
    })

    @api.doc(security=None, body=worker_auth)
    def post(self):
        args = api.payload
        user = Worker.query.filter(Worker.user == args['username']).one_or_none()
        if user and user.check_password(args['password']):
            return {'token': jwt.encode({
                'id': str(user.id),
                'username': user.user,
                'since': str(datetime.utcnow().isoformat())
            }, app.config['SECRET_KEY'], algorithm=AuthService.algorithm).decode('utf-8')}, 200
        return {}, 501
