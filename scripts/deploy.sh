#!/bin/sh
DOCKER_HUB_USERNAME=$1
DOCKER_HUB_PASSWORD=$2
docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD
docker-compose up -d
