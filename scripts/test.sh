source venv/bin/activate
PYTHONPATH=$(pwd)
coverage erase
coverage run manage.py test
coverage xml -i
sonar-scanner -D"sonar.login=$SONAR_LOGIN" -D"sonar.branch.name=$BRANCH" -D"sonar.host.url=$SONAR_URL"